package cipher.substitution;

import cipher.Cipher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Homophone implements Cipher {

    private static final int TOTAL = 1000;
    private static final String FORMAT = "%03d";
    private static final String DELIM = " ";
    private final Map<Character, Integer[]> equivalent;

    public Homophone(List<Character> alphabet) {
        final List<Integer> all = new ArrayList<>(TOTAL);
        for (int i = 0; i < TOTAL; i++) {
            all.add(i);
        }
        final Random random = new Random(System.currentTimeMillis());
        equivalent = new HashMap<Character, Integer[]>();
        for (Character ch : alphabet) {
            final Integer[] changes = new Integer[2];
            changes[0] = all.remove(random.nextInt(all.size()));
            changes[1] = all.remove(random.nextInt(all.size()));
            equivalent.put(ch, changes);
        }
    }

    @Override
    public String encrypt(String msg) {
        final Random random = new Random(System.currentTimeMillis());
        final StringBuilder encrypted = new StringBuilder();
        for (Character ch : msg.toCharArray()) {
            encrypted.append(String.format(FORMAT, equivalent.get(ch)[random.nextInt(2)])).append(DELIM);
        }
        return encrypted.toString().trim();
    }

    @Override
    public String decrypt(String msg) {
        final StringBuilder decrypted = new StringBuilder();
        for (String point : msg.split(DELIM)) {
            final Integer value = Integer.valueOf(point);
outer:      for (Map.Entry<Character, Integer[]> entry : equivalent.entrySet()) {
                for (Integer code : entry.getValue()) {
                    if (value.equals(code)) {
                        decrypted.append(entry.getKey());
                        break outer;
                    }
                }
            }
        }
        return decrypted.toString();
    }
}
