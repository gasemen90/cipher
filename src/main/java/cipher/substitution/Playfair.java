package cipher.substitution;

import cipher.Cipher;

import java.util.ArrayList;
import java.util.List;

public class Playfair implements Cipher {

    private final int rows;
    private final int columns;
    private final List<Character> replacement;
    private final char dummy;
    private static final String DELIM = " ";

    public Playfair(String slogan, int columns, List<Character> alphabet) {
        dummy = alphabet.get(alphabet.size() - 1);
        this.columns = columns;
        rows = (alphabet.size() - 1) / columns + 1;
        replacement = new ArrayList<>();
        for (char ch : slogan.toCharArray()) {
            if (!replacement.contains(ch)) {
                replacement.add(ch);
            }
        }
        for (char ch : alphabet) {
            if (!replacement.contains(ch)) {
                replacement.add(ch);
            }
        }
        assert replacement.size() == alphabet.size();
    }

    @Override
    public String encrypt(String msg) {
        final StringBuilder encrypted = new StringBuilder();
        for (Bigram bigram : getBigramms(msg, dummy)) {
            final int firstIndex = replacement.indexOf(bigram.first);
            int firstRow  = firstIndex / columns;
            int firstCol  = firstIndex % columns;
            final int secondIndex = replacement.indexOf(bigram.second);
            int secondRow = secondIndex / columns;
            int secondCol = secondIndex % columns;

            if (firstRow == secondRow) {
                final int offset = firstRow * columns;
                do {
                    firstCol = ++firstCol % columns;
                } while (offset + firstCol >= replacement.size());
                bigram.first  = replacement.get(offset + firstCol);
                do {
                    secondCol = ++secondCol % columns;
                } while (offset + secondCol >= replacement.size());
                bigram.second = replacement.get(offset + secondCol);
            } else if (firstCol == secondCol) {
                do {
                    firstRow = ++firstRow % rows;
                } while (firstRow * columns + firstCol >= replacement.size());
                bigram.first  = replacement.get(firstRow * columns + firstCol);
                do {
                    secondRow = ++secondRow % rows;
                } while (secondRow * columns + secondCol >= replacement.size());
                bigram.second = replacement.get(secondRow * columns + secondCol);
            } else {
                bigram.first  = replacement.get(Math.min(firstRow  * columns + secondCol, replacement.size() - 1));
                bigram.second = replacement.get(Math.min(secondRow * columns + firstCol, replacement.size() - 1));
            }
            encrypted.append(bigram.toString()).append(DELIM);
        }
        return encrypted.toString().trim();
    }

    @Override
    public String decrypt(String msg) {
        final StringBuilder decrypted = new StringBuilder();
        for (String bigram : msg.split(DELIM)) {
            assert bigram.length() == 2;
            final int firstIndex = replacement.indexOf(bigram.charAt(0));
            int firstRow  = firstIndex / columns;
            int firstCol  = firstIndex % columns;
            final int secondIndex = replacement.indexOf(bigram.charAt(1));
            int secondRow = secondIndex / columns;
            int secondCol = secondIndex % columns;
            char first;
            char second;
            if (firstRow == secondRow) {
                final int offset = firstRow * columns;
                do {
                    firstCol = (firstCol - 1 + columns) % columns;
                } while (offset + firstCol >= replacement.size());
                first = replacement.get(offset + firstCol);
                do {
                    secondCol = (secondCol - 1 + columns) % columns;
                } while (offset + secondCol >= replacement.size());
                second = replacement.get(offset + secondCol);
            } else if (firstCol == secondCol) {
                do {
                    firstRow = (firstRow - 1 + rows) % rows;
                } while (firstRow * columns + firstCol >= replacement.size());
                first = replacement.get(firstRow * columns + firstCol);
                do {
                    secondRow = (secondRow - 1 + rows) % rows;
                } while (secondRow * columns + secondCol >= replacement.size());
                second = replacement.get(secondRow * columns + secondCol);
            } else {
                first  = replacement.get(Math.min(firstRow  * columns + secondCol, replacement.size() - 1));
                second = replacement.get(Math.min(secondRow * columns + firstCol, replacement.size() - 1));
            }
            decrypted.append(first).append(second);
        }
        return decrypted.toString();
    }

    private static List<Bigram> getBigramms(String msg, char dummy) {
        msg = msg.replaceAll(" ", "");
        final List<Bigram> bigrams = new ArrayList<>();
        int i;
        for (i = 0; i < msg.length() - 1; i += 2) {
            final Bigram bigram = new Bigram();
            bigram.first = msg.charAt(i);
            if (msg.charAt(i) == msg.charAt(i + 1)) {
                bigram.second = dummy;
                i--;
            } else {
                bigram.second = msg.charAt(i + 1);
            }
            bigrams.add(bigram);
        }
        if (i < msg.length()) {
            bigrams.add(new Bigram(msg.charAt(i), dummy));
        }
        return bigrams;
    }

    private static class Bigram {
        char first;
        char second;

        Bigram() {}
        Bigram(char first, char second) {
            this.first = first;
            this.second = second;
        }

        @Override
        public String toString() {
            return "" + first + second;
        }
    }
}
