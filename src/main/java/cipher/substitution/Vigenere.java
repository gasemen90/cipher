package cipher.substitution;

import cipher.Cipher;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class Vigenere implements Cipher {

    private final List<Character> alphabet;
    private final char[][] trismusTable;
    private final int[] key;

    public Vigenere(List<Character> alphabet) {
        this.alphabet = alphabet;
        trismusTable = getTrismusTable(alphabet);
        final List<Character> alphabetCopy = new ArrayList<Character>(alphabet);
        final Random random = new Random(System.currentTimeMillis());
        final StringBuilder keyBuilder = new StringBuilder();
        for (int i = 0; i < alphabet.size(); i++) {
            keyBuilder.append( alphabetCopy.remove(random.nextInt( alphabetCopy.size())));
        }
        final String key = keyBuilder.toString();
        this.key = new int[alphabet.size()];
        for (int i = 0; i < key.length(); i++) {
            this.key[i] = alphabet.indexOf(key.charAt(i));
        }
    }

    public Vigenere(String key, List<Character> alphabet) {
        this.alphabet = alphabet;
        trismusTable = getTrismusTable(alphabet);
        this.key = new int[alphabet.size()];
        for (int i = 0; i < alphabet.size(); i++) {
            this.key[i] = alphabet.indexOf(key.charAt(i % key.length()));
        }
    }

    @Override
    public String encrypt(String msg) {
        final StringBuilder encoded = new StringBuilder();
        for (int i = 0; i < msg.length(); i++) {
            encoded.append(trismusTable[key[i % key.length]][alphabet.indexOf(msg.charAt(i))]);
        }
        return encoded.toString();
    }

    @Override
    public String decrypt(String msg) {
        final StringBuilder decoded = new StringBuilder();
        for (int i = 0; i < msg.length(); i++) {
            char ch = msg.charAt(i);
            char[] line = trismusTable[key[i % key.length]];
            for (int j = 0; j < alphabet.size(); j++) {
                if (ch == line[j]) {
                    decoded.append(alphabet.get(j));
                    break;
                }
            }
        }
        return decoded.toString();
    }

    protected static char[][] getTrismusTable(List<Character> alphabet) {
        final char[][] trismusTable = new char[alphabet.size()][];
        for (int i = 0; i < alphabet.size(); i++) {
            char[] line = new char[alphabet.size()];
            for (int j = 0; j < alphabet.size(); j++) {
                line[j] = alphabet.get((i + j) % alphabet.size());
            }
            trismusTable[i] = line;
        }
        return trismusTable;
    }
}
