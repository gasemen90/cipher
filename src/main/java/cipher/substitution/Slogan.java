package cipher.substitution;

import cipher.Cipher;

import java.util.ArrayList;
import java.util.List;

public class Slogan implements Cipher {

    private final List<Character> alphabet;
    private final List<Character> replacement;

    public Slogan(String slogan, List<Character> alphabet) {
        this.alphabet = alphabet;
        replacement = new ArrayList<>();
        for (char ch : slogan.toCharArray()) {
            if (!replacement.contains(ch)) {
                replacement.add(ch);
            }
        }
        for (char ch : alphabet) {
            if (!replacement.contains(ch)) {
                replacement.add(ch);
            }
        }
        assert replacement.size() == alphabet.size();
    }

    @Override
    public String encrypt(String msg) {
        final StringBuilder encryptedMsg = new StringBuilder();
        for (char ch : msg.toCharArray()) {
            encryptedMsg.append(replacement.get(alphabet.indexOf(ch)));
        }
        return encryptedMsg.toString();
    }

    @Override
    public String decrypt(String msg) {
        final StringBuilder decryptedMsg = new StringBuilder();
        for (char ch : msg.toCharArray()) {
            decryptedMsg.append(alphabet.get(replacement.indexOf(ch)));
        }
        return decryptedMsg.toString();
    }
}
