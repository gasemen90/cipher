package cipher.substitution;

import cipher.Cipher;

import java.util.List;

public class PolybiusSquare implements Cipher {

    private static final String DELIM = " ";

    private final List<Character> alphabet;
    private final int columns;

    public PolybiusSquare(int columns, List<Character> alphabet) {
        assert columns < 10;
        this.alphabet = alphabet;
        this.columns = columns;
        assert (alphabet.size() - 1) / columns + 1 < 10;
    }

    @Override
    public String encrypt(String msg) {
        final StringBuilder encryptedMsg = new StringBuilder();
        for (char ch : msg.toCharArray()) {
            int row = (alphabet.indexOf(ch) / columns) + 1;
            int col = (alphabet.indexOf(ch) % columns) + 1;
            encryptedMsg.append(row * 10 + col).append(DELIM);
        }
        return encryptedMsg.toString().trim();
    }

    @Override
    public String decrypt(String msg) {
        final StringBuilder decryptedMsg = new StringBuilder();
        for (String coordStr : msg.split(DELIM)) {
            int coord = Integer.valueOf(coordStr);
            int row = coord / 10;
            int column = coord % 10;
            decryptedMsg.append(alphabet.get((row - 1) * columns + column - 1));
        }
        return decryptedMsg.toString();
    }
}
