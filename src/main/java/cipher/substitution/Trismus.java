package cipher.substitution;

import cipher.Cipher;

import java.util.ArrayList;
import java.util.List;

public class Trismus implements Cipher {

    private int rows;
    private int columns;
    private List<Character> replacement;

    public Trismus(String slogan, int columns, List<Character> alphabet) {
        assert slogan.length() <= alphabet.size();
        this.rows = (alphabet.size() - 1) / columns + 1;
        this.columns = columns;
        replacement = new ArrayList<Character>();
        for (char ch : slogan.toCharArray()) {
            if (!replacement.contains(ch)) {
                replacement.add(ch);
            }
        }
        for (char ch : alphabet) {
            if (!replacement.contains(ch)) {
                replacement.add(ch);
            }
        }
        assert replacement.size() == alphabet.size();
    }

    @Override
    public String encrypt(String msg) {
        final StringBuilder encrypted = new StringBuilder();
        for (char ch : msg.toCharArray()) {
            int col = replacement.indexOf(ch) % columns;
            int row = (replacement.indexOf(ch) / columns + 1) % rows;
            if (row * columns + col >= replacement.size()) {
                row = ++row % rows;
            }
            encrypted.append(replacement.get(row * columns + col));
        }
        return encrypted.toString();
    }

    @Override
    public String decrypt(String msg) {
        final StringBuilder decrypted = new StringBuilder();
        for (char ch : msg.toCharArray()) {
            int col = replacement.indexOf(ch) % columns;
            int row = (replacement.indexOf(ch) / columns - 1 + rows) % rows;
            if (row * columns + col >= replacement.size()) {
                row = --row % rows;
            }
            decrypted.append(replacement.get(row * columns + col));
        }
        return decrypted.toString();
    }
}
