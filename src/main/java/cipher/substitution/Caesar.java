package cipher.substitution;

import cipher.Cipher;

import java.util.List;

public class Caesar implements Cipher {

    private final List<Character> alphabet;
    private final int shift;

    public Caesar(int shift, List<Character> alphabet) {
        this.shift = shift;
        this.alphabet = alphabet;
    }

    @Override
    public String encrypt(String msg) {
        final StringBuilder encryptedMsg = new StringBuilder();
        for (char ch : msg.toCharArray()) {
            encryptedMsg.append(alphabet.get((alphabet.indexOf(ch) + shift) % alphabet.size()));
        }
        return encryptedMsg.toString();
    }

    @Override
    public String decrypt(String msg) {
        final StringBuilder decryptedMsg = new StringBuilder();
        for (char ch : msg.toCharArray()) {
            decryptedMsg.append(alphabet.get((alphabet.size() + alphabet.indexOf(ch) - shift) % alphabet.size()));
        }
        return decryptedMsg.toString();
    }
}
