package cipher.publickey;

import cipher.Cipher;
import cipher.symmetric.FixedBitSet;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.Random;

import static cipher.Utils.bytesToString;
import static cipher.Utils.reverseArray;
import static cipher.Utils.stringToBytes;

public class Knapsack implements Cipher {

    private static final int BOUND = 0xff;
    private static final int LIMIT = 104; // % 8 == 0

    private final BigInteger[] w; // private key
    private final BigInteger[] b; // public key
    private final BigInteger q;
    private final BigInteger c;

    public Knapsack() {
        final Random random = new Random(System.currentTimeMillis());
        w = new BigInteger[LIMIT];
        BigInteger sum = BigInteger.ZERO;
        for (int i = 0; i < LIMIT; i++) {
            sum = sum.add(sum).add(BigInteger.valueOf(random.nextInt(BOUND)));
            w[i] = sum;
        }
        q = sum.add(sum).nextProbablePrime();
        final int qBits = q.bitLength() - 1;
        BigInteger r = new BigInteger(qBits, random);
        while (!q.gcd(r).equals(BigInteger.ONE)) {
            r = new BigInteger(qBits, random);
        }
        b = new BigInteger[LIMIT];
        for (int i = 0; i < LIMIT; i++) {
            b[i] = r.multiply(w[i]).mod(q);
        }
        c = r.modInverse(q);
    }

    @Override
    public String encrypt(String msg) {
        final StringBuilder result = new StringBuilder();
        final FixedBitSet bitSet = FixedBitSet.valueOf(reverseArray(stringToBytes(msg)));
        for (int i = 0; i < bitSet.length(); i += LIMIT) {
            final int limit = i + Math.min(LIMIT, bitSet.length() - i);
            BigInteger sum = BigInteger.ZERO;
            for (int j = i; j < limit; j++) {
                if (bitSet.get(j)) {
                    sum = sum.add(b[j - i]);
                }
            }
            result.append(sum.toString(16)).append(' ');
        }
        return result.toString().trim();
    }

    @Override
    public String decrypt(String msg) {
        final String[] chunks = msg.split(" ");
        final ByteBuffer bb = ByteBuffer.allocate(LIMIT * chunks.length);
        for (int i = 0; i < chunks.length; i++) {
            BigInteger a = new BigInteger(chunks[i], 16).multiply(c).mod(q);
            final BitSet bs = new BitSet();
            for (int j = w.length - 1; j >= 0 && !a.equals(BigInteger.ZERO); j--) {
                if (w[j].compareTo(a) <= 0) {
                    bs.set(j);
                    a = a.subtract(w[j]);
                }
            }
            bb.put(bs.toByteArray());
        }
        return bytesToString(reverseArray(bb.array())).trim();
    }
}