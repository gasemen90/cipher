package cipher.publickey.rsa;

import cipher.Cipher;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;

import static cipher.Utils.bytesToString;
import static cipher.Utils.reverseArray;
import static cipher.Utils.stringToBytes;

public class RSA implements Cipher {

    protected static final BigInteger MIN_E = BigInteger.valueOf(65537);

    private final KeyPair keyPair;

    public RSA() {
        this(32);
    }

    public RSA(int nBits) {
        assert nBits >= 16;
        keyPair = generateKeyPair(nBits);
    }

    @Override
    public String encrypt(String msg) {
        final byte[] bytes = reverseArray(stringToBytes(msg));
        int chunkSize = (keyPair.publicKey.modulus.bitLength() - 1) / Byte.SIZE;
        if (chunkSize % 2 != 0) {
            chunkSize -= 1;
        }
        assert chunkSize > 0;
        final int chunkCount = bytes.length / chunkSize;
        final StringBuilder result = new StringBuilder();
        for (int i = 0; i < chunkCount; i++) {
            final BigInteger a = new BigInteger(Arrays.copyOfRange(bytes, i * chunkSize, (i + 1) * chunkSize));
            result.append(a.modPow(keyPair.publicKey.exponent, keyPair.publicKey.modulus).toString(16)).append(' ');
        }
        if (chunkCount * chunkSize == bytes.length) {
            return result.toString().trim();
        }
        final BigInteger a = new BigInteger(Arrays.copyOfRange(bytes, chunkCount * chunkSize, bytes.length));
        result.append(a.modPow(keyPair.publicKey.exponent, keyPair.publicKey.modulus).toString(16));
        return result.toString().trim();
    }

    @Override
    public String decrypt(String msg) {
        int chunkSize = (keyPair.privateKey.modulus.bitLength() - 1) / Byte.SIZE;
        if (chunkSize % 2 != 0) {
            chunkSize -= 1;
        }
        assert chunkSize > 0;
        final String[] chunks = msg.split(" ");
        final ByteBuffer bytes = ByteBuffer.allocate(chunkSize * chunks.length);
        for (int i = 0; i < chunks.length; i++) {
            bytes.put(new BigInteger(chunks[i], 16).modPow(keyPair.privateKey.exponent, keyPair.privateKey.modulus).toByteArray());
        }
        return bytesToString(reverseArray(Arrays.copyOfRange(bytes.array(), 0, bytes.position())));
    }

    public static KeyPair generateKeyPair(int bitLength) {
        assert bitLength >= 8;
        final Random random = new Random(System.currentTimeMillis());
        BigInteger p = BigInteger.probablePrime(bitLength, random);
        BigInteger q;
        do {
            q = BigInteger.probablePrime(bitLength, random);
        } while (p.equals(q));
        BigInteger n = p.multiply(q);
        BigInteger phi = n.subtract(p).subtract(q).add(BigInteger.ONE);
        final int eLength = n.bitLength() - 1;
        BigInteger e;
        do {
            e = new BigInteger(eLength, random);
        } while ((!phi.gcd(e).equals(BigInteger.ONE) || e.compareTo(MIN_E) < 0 || !e.testBit(0)));
        return new KeyPair(new Key(n, e), new Key(n, e.modInverse(phi)));
    }
}
