package cipher.publickey.rsa;

public class KeyPair {

    public final Key publicKey;
    public final Key privateKey;

    public KeyPair(Key publicKey, Key privateKey) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }
}
