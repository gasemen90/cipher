package cipher.publickey.rsa;

import java.math.BigInteger;

public class Key {

    public final BigInteger modulus;
    public final BigInteger exponent;

    public Key(BigInteger modulus, BigInteger exponent) {
        this.modulus = modulus;
        this.exponent = exponent;
    }
}
