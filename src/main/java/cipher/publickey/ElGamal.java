package cipher.publickey;

import cipher.Cipher;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static cipher.Utils.bytesToString;
import static cipher.Utils.reverseArray;
import static cipher.Utils.stringToBytes;

public class ElGamal implements Cipher {

    private static final int BLOCK = 0xffff * Byte.SIZE;

    private final BigInteger p; // public group key
    private final BigInteger g; // public group key
    private final BigInteger x; // private key
    private final BigInteger y; // public key


    // http://ftp.icm.edu.pl/packages/replay.old/libraries/aes/java/elGamal.java
    public ElGamal() {
        final Random random = new Random(System.currentTimeMillis());
        p = new BigInteger(Integer.SIZE, random).nextProbablePrime();
        assert p.compareTo(BigInteger.valueOf(BLOCK)) > 0;
        g = findPrimitiveRootOfPrime(p);
        BigInteger tempX;
        do {
            tempX = new BigInteger(p.bitLength(), random);
        } while (tempX.compareTo(p) >= 0 || tempX.compareTo(BigInteger.ONE) < 0);
        x = tempX;
        y = g.modPow(x, p);
    }

    @Override
    public String encrypt(String msg) {
        final byte[] bytes = reverseArray(stringToBytes(msg));
        final Random random = new Random(System.currentTimeMillis());
        final StringBuilder result = new StringBuilder();
        BigInteger k;
        for (int i = 0; i < bytes.length; i += BLOCK) {
            do {
                k = new BigInteger(p.bitLength(), random);
            } while (k.compareTo(p) >= 0 || k.compareTo(BigInteger.ONE) <= 0);
            result.append(g.modPow(k, p).toString(16)).append(' ');
            result.append(new BigInteger(Arrays.copyOfRange(bytes, i, i + Math.min(BLOCK, bytes.length - i))).xor(y.modPow(k, p)).toString(16)).append(' ');
        }
        return result.toString().trim();
    }

    @Override
    public String decrypt(String msg) {
        final String[] chunks = msg.split(" ");
        final ByteBuffer bb = ByteBuffer.allocate(BLOCK * (chunks.length / 2));
        BigInteger a, b;
        for (int i = 0; i < chunks.length; i += 2) {
            a = new BigInteger(chunks[i], 16);
            b = new BigInteger(chunks[i + 1], 16);
            bb.put(b.xor(a.modPow(x, p)).toByteArray());
        }
        return bytesToString(reverseArray(bb.array())).trim();
    }

    // http://e-maxx.ru/algo/primitive_root
    private static BigInteger findPrimitiveRootOfPrime(BigInteger p) {
        final BigInteger phi = p.subtract(BigInteger.ONE);
        BigInteger n = phi;
        final List<BigInteger> fact = new ArrayList<BigInteger>();
        for (BigInteger i = BigInteger.valueOf(2); i.multiply(i).compareTo(n) <= 0; i = i.add(BigInteger.ONE)) {
            if (n.remainder(i).equals(BigInteger.ZERO)) {
                fact.add(i);
                while (n.remainder(i).equals(BigInteger.ZERO)) {
                    n = n.divide(i);
                }
            }
        }
        if (n.compareTo(BigInteger.ONE) > 0) {
            fact.add(n);
        }
        for (BigInteger res = BigInteger.valueOf(2); res.compareTo(p) < 0; res = res.add(BigInteger.ONE)) {
            boolean ok = true;
            for (int i = 0; i < fact.size() && ok; i++) {
                ok = !res.modPow(phi.divide(fact.get(i)), p).equals(BigInteger.ONE);
            }
            if (ok) {
                return res;
            }
        }
        throw new RuntimeException("Can't find primitive root for: " + p);
    }
}