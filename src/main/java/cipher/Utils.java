package cipher;

public class Utils {

    public static byte[] stringToBytes(String str) {
        final char[] buffer = str.toCharArray();
        final byte[] bytes = new byte[buffer.length << 1];
        int bpos;
        char ch;
        for (int i = 0; i < buffer.length; i++) {
            bpos = i << 1;
            ch = buffer[i];
            bytes[bpos] = (byte) ((ch & 0xFF00) >> 8);
            bytes[bpos + 1] = (byte) (ch & 0x00FF);
        }
        return bytes;
    }

    public static String bytesToString(byte[] bytes) {
        final char[] buffer = new char[bytes.length >> 1];
        int bpos;
        for (int i = 0; i < buffer.length; i++) {
            bpos = i << 1;
            buffer[i] = (char) (((bytes[bpos] & 0x00FF) << 8) + (bytes[bpos + 1] & 0x00FF));
        }
        return new String(buffer);
    }

    public static byte[] reverseArray(byte[] bytes) {
        for (int i = 0; i < bytes.length / 2; i++) {
            byte temp = bytes[i];
            bytes[i] = bytes[bytes.length - i - 1];
            bytes[bytes.length - i - 1] = temp;
        }
        return bytes;
    }
}
