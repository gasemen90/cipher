package cipher;

public interface Cipher {

    // зашифровать
    String encrypt(String msg);
    // расшифровать
    String decrypt(String msg);

}
