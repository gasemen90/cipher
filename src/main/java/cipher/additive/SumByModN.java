package cipher.additive;

import java.util.List;

public class SumByModN extends AbstractSumByMod {

    private final List<Character> alphabet;

    public SumByModN(String gamma, List<Character> alphabet) {
        super(gamma);
        this.alphabet = alphabet;
    }

    protected char getEncoded(char msg, char gamma, int i) {
        return alphabet.get((alphabet.indexOf(msg) + alphabet.indexOf(gamma)) % alphabet.size());
    }

    protected char getDecoded(char msg, char gamma, int i) {
        return alphabet.get((alphabet.indexOf(msg) + alphabet.size() - alphabet.indexOf(gamma)) % alphabet.size());
    }
}
