package cipher.additive;

public class SumByMod2 extends AbstractSumByMod {

    public SumByMod2(String gamma) {
        super(gamma);
    }

    protected char getDecoded(char msg, char gamma, int i) {
        return (char) (msg ^ gamma);
    }

    protected char getEncoded(char msg, char gamma, int i) {
        return (char) (msg ^ gamma);
    }
}
