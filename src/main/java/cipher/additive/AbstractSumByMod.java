package cipher.additive;

import cipher.Cipher;


abstract public class AbstractSumByMod implements Cipher {

    private final String gamma;

    protected AbstractSumByMod(String gamma) {
        this.gamma = gamma;
    }

    @Override
    public String encrypt(String msg) {
        final String localGamma = getLocalGamma(msg.length());
        final char[] encrypted = new char[msg.length()];
        for (int i = 0; i < encrypted.length; i++) {
            encrypted[i] = getEncoded(msg.charAt(i), localGamma.charAt(i), i);
        }
        return new String(encrypted);
    }

    @Override
    public String decrypt(String msg) {
        final String localGamma = getLocalGamma(msg.length());
        final char[] decrypted = new char[msg.length()];
        for (int i = 0; i < decrypted.length; i++) {
            decrypted[i] = getDecoded(msg.charAt(i), localGamma.charAt(i), i);
        }
        return new String(decrypted);
    }

    abstract protected char getEncoded(char msg, char gamma, int i);
    abstract protected char getDecoded(char msg, char gamma, int i);

    private String getLocalGamma(int msgLength) {
        if (msgLength > gamma.length()) {
            String localGamma = gamma;
            final int fill = msgLength - gamma.length();
            for (int i = 0; i < fill / gamma.length(); i++) {
                localGamma += gamma;
            }
            if (fill % gamma.length() != 0) {
                localGamma += gamma.substring(0, fill % gamma.length());
            }
            return localGamma;
        } else {
            return gamma;
        }
    }

}
