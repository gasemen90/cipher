package cipher.symmetric;

import java.nio.ByteBuffer;
import java.nio.LongBuffer;
import java.util.BitSet;

public class FixedBitSet extends BitSet {

    private int length;

    public FixedBitSet(int nBits) {
        super(nBits);
        this.length = nBits;
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= length) {
            throw new IndexOutOfBoundsException();
        }
    }

    private void checkRangeIndex(int fromIndex, int toIndex) {
        if (fromIndex < 0 || fromIndex >= length || toIndex < 0 || toIndex > length) {
            throw new IndexOutOfBoundsException();
        }
    }


    @Override
    public void flip(int bitIndex) {
        checkIndex(bitIndex);
        super.flip(bitIndex);
    }

    @Override
    public void flip(int fromIndex, int toIndex) {
        checkRangeIndex(fromIndex, toIndex);
        super.flip(fromIndex, toIndex);
    }

    @Override
    public void set(int bitIndex) {
        checkIndex(bitIndex);
        super.set(bitIndex);
    }

    @Override
    public void set(int bitIndex, boolean value) {
        checkIndex(bitIndex);
        super.set(bitIndex, value);
    }

    @Override
    public void set(int fromIndex, int toIndex) {
        checkRangeIndex(fromIndex, toIndex);
        super.set(fromIndex, toIndex);
    }

    @Override
    public void set(int fromIndex, int toIndex, boolean value) {
        checkRangeIndex(fromIndex, toIndex);
        super.set(fromIndex, toIndex, value);
    }

    @Override
    public void clear(int bitIndex) {
        checkIndex(bitIndex);
        super.clear(bitIndex);
    }

    @Override
    public void clear(int fromIndex, int toIndex) {
        checkRangeIndex(fromIndex, toIndex);
        super.clear(fromIndex, toIndex);
    }

    @Override
    public boolean get(int bitIndex) {
        checkIndex(bitIndex);
        return super.get(bitIndex);
    }

    @Override
    public FixedBitSet get(int fromIndex, int toIndex) {
        checkRangeIndex(fromIndex, toIndex);
        final BitSet bitSet = super.get(fromIndex, toIndex);
        final FixedBitSet result = new FixedBitSet(toIndex - fromIndex);
        for (int i = bitSet.nextSetBit(0); i >= 0; i = bitSet.nextSetBit(i + 1)) {
            result.set(i);
        }
        return result;
    }

    @Override
    public int length() {
        return length;
    }

    public void appendRight(FixedBitSet bitSet) {
        final FixedBitSet self = clone();
        clear();
        length += bitSet.length();
        for (int i = bitSet.nextSetBit(0); i >= 0; i = bitSet.nextSetBit(i + 1)) {
            set(i);
        }
        for (int i = self.nextSetBit(0); i >= 0; i = self.nextSetBit(i + 1)) {
            set(i + bitSet.length());
        }
    }

    public void appendRight(boolean value) {
        final FixedBitSet self = clone();
        clear();
        length += 1;
        set(0, value);
        for (int i = self.nextSetBit(0); i >= 0; i = self.nextSetBit(i + 1)) {
            set(i + 1);
        }
    }

    public void appendLeft(FixedBitSet bitSet) {
        final int oldLength = length;
        length += bitSet.length();
        for (int i = bitSet.nextSetBit(0); i >= 0; i = bitSet.nextSetBit(i + 1)) {
            set(oldLength + i);
        }
    }

    public void appendLeft(boolean value) {
        length += 1;
        set(length - 1, value);
    }

    public void shiftRight(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }
        final FixedBitSet self = clone();
        clear();
        for (int i = self.nextSetBit(n); i >= 0; i = self.nextSetBit(i + 1)) {
            set(i - n, self.get(i));
        }
    }

    public void shiftLeft(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }
        final FixedBitSet self = clone();
        clear();
        for (int i = self.nextSetBit(0); i >= 0 && i < length - n; i = self.nextSetBit(i + 1)) {
            set(i + n, self.get(i));
        }
    }

    public void rotateRight(int n) {
        final BitSet head = get(0, n);
        shiftRight(n);
        for (int i = head.nextSetBit(0); i >= 0; i = head.nextSetBit(i + 1)) {
            set(length - n + i, head.get(i));
        }
    }

    public void rotateLeft(int n) {
        final BitSet tail = get(length - n, length);
        shiftLeft(n);
        for (int i = tail.nextSetBit(0); i >= 0; i = tail.nextSetBit(i + 1)) {
            set(i, tail.get(i));
        }
    }

    @Override
    public FixedBitSet clone() {
        final FixedBitSet fixedBitSet = new FixedBitSet(length);
        for (int i = nextSetBit(0); i >= 0; i = nextSetBit(i + 1)) {
            fixedBitSet.set(i);
        }
        return fixedBitSet;
    }

    public int toInt() {
        int value = 0;
        for (int i = nextSetBit(0); i >= 0; i = nextSetBit(i + 1)) {
            value += get(i) ? (1 << i) : 0;
        }
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FixedBitSet) || length != ((FixedBitSet) obj).length) {
            return false;
        }
        return super.equals(obj);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for (int i = length - 1; i >= 0; i--) {
            sb.append(get(i) ? '1' : '0');
        }
        return sb.toString();
    }

    public static FixedBitSet concat(FixedBitSet bitSet1, FixedBitSet bitSet2) {
        final FixedBitSet result = new FixedBitSet(bitSet1.length() + bitSet2.length());
        for (int i = bitSet1.nextSetBit(0); i >= 0; i = bitSet1.nextSetBit(i + 1)) {
            result.set(i);
        }
        for (int i = bitSet2.nextSetBit(0); i >= 0; i = bitSet2.nextSetBit(i + 1)) {
            result.set(bitSet1.length() + i);
        }
        return result;
    }

    public static FixedBitSet valueOf(byte[] bytes) {
        final FixedBitSet fixedBitSet = new FixedBitSet(bytes.length * Byte.SIZE);
        final BitSet bitSet = BitSet.valueOf(bytes);
        for (int i = bitSet.nextSetBit(0); i >= 0; i = bitSet.nextSetBit(i + 1)) {
            fixedBitSet.set(i);
        }
        return fixedBitSet;
    }

    public static FixedBitSet valueOf(ByteBuffer byteBuffer) {
        return valueOf(byteBuffer.array());
    }

    public static FixedBitSet valueOf(long[] longs) {
        final FixedBitSet fixedBitSet = new FixedBitSet(longs.length * Long.SIZE);
        final BitSet bitSet = BitSet.valueOf(longs);
        for (int i = bitSet.nextSetBit(0); i >= 0; i = bitSet.nextSetBit(i + 1)) {
            fixedBitSet.set(i);
        }
        return fixedBitSet;
    }

    public static FixedBitSet valueOf(LongBuffer longBuffer) {
        return valueOf(longBuffer.array());
    }
}
