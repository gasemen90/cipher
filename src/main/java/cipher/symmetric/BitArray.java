package cipher.symmetric;

import java.util.Arrays;
import java.util.BitSet;

public class BitArray {

    private final boolean[] arr;

    public BitArray(int size) {
        arr = new boolean[size];
        Arrays.fill(arr, false);
    }

    public BitArray(boolean[] arr) {
        this.arr = arr;
    }

    public BitArray(byte[] arr) {
        this.arr = new boolean[arr.length * 8];
        Arrays.fill(this.arr, false);
        final BitSet bs = BitSet.valueOf(arr);
        for (int i = bs.nextSetBit(0); i >= 0; i = bs.nextSetBit(i + 1)) {
            this.arr[i] = true;
        }
    }

    public BitArray(BitArray bitArray) {
        arr = new boolean[bitArray.length()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = bitArray.get(i);
        }
    }

    public BitArray appendRight(BitArray array) {
        final BitArray result = new BitArray(array.length() + length());
        for (int i = 0; i < array.length(); i++) {
            result.set(i, array.get(i));
        }
        for (int i = 0; i < length(); i++) {
            result.set(i + array.length(), get(i));
        }
        return result;
    }

    public BitArray appendRight(boolean value) {
        return appendRight(new BitArray(new boolean[] { value }));
    }

    public BitArray appendLeft(BitArray array) {
        final BitArray result = new BitArray(array.length() + length());
        for (int i = 0; i < length(); i++) {
            result.set(i, get(i));
        }
        for (int i = 0; i < array.length(); i++) {
            result.set(i + length(), array.get(i));
        }
        return result;
    }

    public BitArray appendLeft(boolean value) {
        return appendLeft(new BitArray(new boolean[] { value }));
    }

    // Performs a logical AND of this target bit set with the argument bit set.
    public BitArray and(BitArray array) {
        final BitArray result = new BitArray(Math.min(arr.length, array.length()));
        for (int i = 0; i < result.length(); i++) {
            result.set(i, get(i) && array.get(i));
        }
        return result;
    }

    // Clears all of the bits in this BitSet whose corresponding bit is set in the specified BitSet.
    public BitArray andNot(BitArray array) {
        final BitArray result = new BitArray(this);
        final int limit = Math.min(result.length(), array.length());
        for (int i = 0; i < limit; i++) {
            if (array.get(i)) {
                result.set(i, false);
            }
        }
        return result;
    }

    // Returns the number of bits set to true in this BitSet.
    public int cardinality() {
        int cardinality = 0;
        for (boolean value : arr) {
            if (value) {
                cardinality++;
            }
        }
        return cardinality;
    }

    public int cardinality(int fromIndex, int toIndex) {
        int cardinality = 0;
        for (int i = fromIndex; i < toIndex; i++) {
            if (arr[i]) {
                cardinality++;
            }
        }
        return cardinality;
    }

    // Sets all of the bits in this BitSet to false.
    public BitArray clear() {
        Arrays.fill(arr, false);
        return this;
    }

    // Sets the bit specified by the index to false.
    public BitArray clear(int bitIndex) {
        set(bitIndex, false);
        return this;
    }

    // Sets the bits from the specified fromIndex (inclusive) to the specified toIndex (exclusive) to false.
    public BitArray clear(int fromIndex, int toIndex) {
        set(fromIndex, toIndex, false);
        return this;
    }

    // Cloning this BitSet produces a new BitSet that is equal to it.
    @Override
    public BitArray clone() {
        return new BitArray(this);
    }

    // Compares this object against the specified object.
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof BitArray)) {
            return false;
        }
        return Arrays.equals(arr, ((BitArray) obj).arr);
    }

    public BitArray reverse() {
        final BitArray result = new BitArray(length());
        final int length = length() - 1;
        for (int i = 0; i < length(); i++) {
            result.set(i, get(length - i));
        }
        return result;
    }

    // Sets the bit at the specified index to the complement of its current value.
    public BitArray flip(int bitIndex) {
        arr[bitIndex] = !arr[bitIndex];
        return this;
    }

    // Sets each bit from the specified fromIndex (inclusive) to the specified toIndex (exclusive) to the complement of its current value.
    public BitArray flip(int fromIndex, int toIndex) {
        for (int i = fromIndex; i < toIndex; i++) {
            arr[i] = !arr[i];
        }
        return this;
    }

    // Returns the value of the bit with the specified index.
    public boolean get(int bitIndex) {
        return arr[bitIndex];
    }

    // Returns a new BitSet composed of bits from this BitSet from fromIndex (inclusive) to toIndex (exclusive).
    public BitArray get(int fromIndex, int toIndex) {
        return new BitArray(Arrays.copyOfRange(arr, fromIndex, toIndex));
    }


    // Returns the hash code value for this bit set.
    @Override
    public int hashCode() {
        return Arrays.hashCode(arr);
    }

    // Returns true if the specified BitSet has any bits set to true that are also set to true in this BitSet.
//    public boolean intersects(BitArray set) {
//
//    }

    // Returns true if this BitSet contains no bits that are set to true.
    public boolean isEmpty() {
        for (int i = 0; i < length(); i++) {
            if (get(i)) {
                return false;
            }
        }
        return true;
    }


    // Returns the "logical size" of this BitSet: the index of the highest set bit in the BitSet plus one.
    public int	length() {
        return arr.length;
    }

//    int	nextClearBit(int fromIndex) {}
//    Returns the index of the first bit that is set to false that occurs on or after the specified starting index.
//    int	nextSetBit(int fromIndex) {}
//    Returns the index of the first bit that is set to true that occurs on or after the specified starting index.

    // Performs a logical OR of this bit set with the bit set argument.
    public BitArray or(BitArray array) {
        final BitArray result = new BitArray(length() > array.length() ? this : array);
        final int limit = Math.min(length(), array.length());
        for (int i = 0; i < limit; i++) {
            result.set(i, get(i) || array.get(i));
        }
        return result;
    }

//    int	previousClearBit(int fromIndex) {}
//    Returns the index of the nearest bit that is set to false that occurs on or before the specified starting index.
//    int	previousSetBit(int fromIndex) {}
//    Returns the index of the nearest bit that is set to true that occurs on or before the specified starting index.

    // Sets the bit at the specified index to true.
    public BitArray set(int bitIndex) {
        set(bitIndex, true);
        return this;
    }

    // Sets the bit at the specified index to the specified value.
    public BitArray set(int bitIndex, boolean value) {
        arr[bitIndex] = value;
        return this;
    }

    // Sets the bits from the specified fromIndex (inclusive) to the specified toIndex (exclusive) to true.
    public BitArray set(int fromIndex, int toIndex) {
        set(fromIndex, toIndex, true);
        return this;
    }

    // Sets the bits from the specified fromIndex (inclusive) to the specified toIndex (exclusive) to the specified value.
    public BitArray set(int fromIndex, int toIndex, boolean value) {
        for (int i = fromIndex; i < toIndex; i++) {
            set(i, value);
        }
        return this;
    }


    // Returns the number of bits of space actually in use by this BitSet to represent bit values.
//    int	size() {
//        return arr.length;
//    }

    // Returns a new byte array containing all the bits in this bit set.
//    byte[] toByteArray() {
//
//    }

    // Returns a new long array containing all the bits in this bit set.
//    long[]	toLongArray() {
//
//    }

    // Returns a string representation of this bit set.
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for (int i = arr.length - 1; i >= 0; i--) {
            sb.append(arr[i] ? '1' : '0');
        }
        return sb.toString();
    }

//    static BitArray	valueOf(byte[] bytes) {}
//    Returns a new bit set containing all the bits in the given byte array.
//    static BitArray	valueOf(ByteBuffer bb) {}
//    Returns a new bit set containing all the bits in the given byte buffer between its position and limit.
//    static BitArray	valueOf(long[] longs) {}
//    Returns a new bit set containing all the bits in the given long array.
//    static BitArray	valueOf(LongBuffer lb) {}
//    Returns a new bit set containing all the bits in the given long buffer between its position and limit.

    // Performs a logical XOR of this bit set with the bit set argument.
    public BitArray xor(BitArray array) {
        final BitArray result = new BitArray(length() > array.length() ? this : array);
        final int limit = Math.min(length(), array.length());
        for (int i = 0; i < limit; i++) {
            result.set(i, get(i) != array.get(i));
        }
        return result;
    }

    public BitArray shiftRight(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }
        final BitArray result = new BitArray(length());
        for (int i = n; i < length(); i++) {
            result.set(i - n, get(i));
        }
        return result;
    }

    public BitArray shiftLeft(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }
        final BitArray result = new BitArray(length());
        for (int i = n; i < result.length(); i++) {
            result.set(i, get(i - n));
        }
        return result;
    }

    public BitArray rotateRight(int n) {
        final boolean[] head = Arrays.copyOfRange(arr, 0, n);
        final BitArray result = shiftRight(n);
        for (int i = 0; i < head.length; i++) {
            result.set(result.length() - n + i, head[i]);
        }
        return result;
    }

    public BitArray rotateLeft(int n) {
        final boolean[] tail = Arrays.copyOfRange(arr, length() - n, length());
        final BitArray result = shiftLeft(n);
        for (int i = 0; i < tail.length; i++) {
            result.set(i, tail[i]);
        }
        return result;
    }

    public byte[] toByteArray() {
        final BitSet bs = new BitSet(arr.length);
        for (int i = 0; i < arr.length; i++) {
            bs.set(i, get(i));
        }
        return bs.toByteArray();
    }
}
