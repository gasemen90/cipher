package cipher.shift;


import cipher.Cipher;

public class Grid implements Cipher {

    private final boolean[][] stencil;
    private final Rotate[] rotates;
    private final Route copying;

    public Grid(boolean[][] stencil, Rotate[] rotates, Route copying) {
        for (boolean[] arr : stencil) {
            assert stencil.length == arr.length;
        }
        this.stencil = stencil;
        this.rotates = rotates;
        this.copying = copying;
    }

    @Override
    public String encrypt(String msg) {
        final char[][] table = new char[stencil.length][stencil.length];
        int num = 0;
        for (int i = 0; i < stencil.length; i++) {
            for (int j = 0; j < stencil[i].length && num < msg.length(); j++) {
                if (stencil[i][j]) {
                    table[i][j] = msg.charAt(num++);
                }
            }
        }
        boolean[][] stencil2 = stencil;
        for (Rotate rotate : rotates) {
            if (Rotate.H == rotate) {
                stencil2 = turnHorizontal(stencil2);
            } else {
                stencil2 = turnVertical(stencil2);
            }
            for (int i = 0; i < stencil2.length; i++) {
                for (int j = 0; j < stencil2[i].length && num < msg.length(); j++) {
                    if (stencil2[i][j]) {
                        table[i][j] = msg.charAt(num++);
                    }
                }
            }
        }
        assert num == msg.length();
        num = 0;
        final char[] encrypted = new char[table.length * table.length];
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                final int index = i * table[i].length + j;
                switch (copying) {
                    case RIGHT_TO_LEFT:
                        encrypted[num++] = table[i][table[i].length - j - 1];
                        break;
                    case LEFT_TO_RIGHT:
                        encrypted[num++] = table[i][j];
                        break;
                    case UP_TO_DOWN:
                        encrypted[num++] = table[index % table.length][index / table.length];
                        break;
                    case DOWN_TO_UP:
                        encrypted[num++] = table[table.length - (index % table.length) - 1][index / table.length];
                        break;
                    default:
                        throw new IllegalStateException();
                }
            }
        }
        return new String(encrypted);
    }

    @Override
    public String decrypt(String msg) {
        int num = 0;
        final char[][] table = new char[stencil.length][stencil.length];
        for (int i = 0; i < stencil.length; i++) {
            for (int j = 0; j < stencil[i].length && num < msg.length(); j++) {
                final int index = i * table[i].length + j;
                switch (copying) {
                    case RIGHT_TO_LEFT:
                        table[i][table[i].length - j - 1] = msg.charAt(num++);
                        break;
                    case LEFT_TO_RIGHT:
                        table[i][j] = msg.charAt(num++);
                        break;
                    case UP_TO_DOWN:
                        table[index % table.length][index / table.length] = msg.charAt(num++);
                        break;
                    case DOWN_TO_UP:
                        table[table.length - (index % table.length) - 1][index / table.length] = msg.charAt(num++);
                        break;
                    default:
                        throw new IllegalStateException();
                }
            }
        }
        boolean[][] stencil2 = stencil;
        for (Rotate rotate : rotates) {
            if (Rotate.H == rotate) {
                stencil2 = turnHorizontal(stencil2);
            } else {
                stencil2 = turnVertical(stencil2);
            }
        }
        final char[] decrypted = new char[table.length * table.length];
        num = decrypted.length;
        for (int i = stencil2.length - 1; i >= 0; i--) {
            for (int j = stencil2[i].length - 1; j >= 0 && num >= 0; j--) {
                if (stencil2[i][j]) {
                    decrypted[--num] = table[i][j];
                }
            }
        }
        for (int i = rotates.length - 1; i >= 0; i--) {
            if (Rotate.H == rotates[i]) {
                stencil2 = turnHorizontal(stencil2);
            } else {
                stencil2 = turnVertical(stencil2);
            }
            for (int j = stencil2.length - 1; j >= 0; j--) {
                for (int k = stencil2[j].length - 1; k >= 0 && num >= 0; k--) {
                    if (stencil2[j][k]) {
                        decrypted[--num] = table[j][k];
                    }
                }
            }
        }
        return new String(decrypted).trim();
    }

    private static boolean[][] turnHorizontal(boolean[][] grid) {
        final boolean[][] newGrid = new boolean[grid.length][];
        for (int i = 0; i < grid.length; i++) {
            newGrid[i] = new boolean[grid[i].length];
            for (int j = 0; j < grid[i].length; j++) {
                newGrid[i][grid[i].length - j - 1] = grid[i][j];
            }
        }
        return newGrid;
    }

    private static boolean[][] turnVertical(boolean[][] grid) {
        final boolean[][] newGrid = new boolean[grid.length][];
        for (int i = 0; i < grid.length; i++) {
            newGrid[grid.length - i - 1] = new boolean[grid[i].length];
            for (int j = 0; j < grid[i].length; j++) {
                newGrid[grid.length - i - 1][j] = grid[i][j];
            }
        }
        return newGrid;
    }

    public enum Rotate {
        H, V
    }
}
