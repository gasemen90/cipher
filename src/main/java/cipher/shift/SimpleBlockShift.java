package cipher.shift;


import cipher.Cipher;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SimpleBlockShift implements Cipher {

    private final int[] transpTable;

    public SimpleBlockShift(int blockSize) {
        transpTable = new int[blockSize];
        final List<Integer> nums = new ArrayList<Integer>();
        for (int i = 0; i < blockSize; i++) {
            nums.add(i);
        }
        final Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < blockSize; i++) {
            transpTable[i] = nums.remove(random.nextInt(nums.size()));
        }
    }

    @Override
    public String encrypt(String msg) {
        if (msg.length() % transpTable.length != 0) {
            final int fill = transpTable.length - msg.length() % transpTable.length;
            final Random random = new Random(System.currentTimeMillis());
            for (int i = 0; i < fill; i++) {
                msg += msg.charAt(random.nextInt(msg.length()));
            }
        }
        final int blockSize = msg.length() / transpTable.length;
        final StringBuilder encrypted = new StringBuilder();
        for (int i = 0; i < blockSize; i++) {
            for (int j = 0; j < transpTable.length; j++) {
                encrypted.append(msg.charAt(i * transpTable.length + transpTable[j]));
            }
        }
        return encrypted.toString();
    }

    @Override
    public String decrypt(String msg) {
        assert msg.length() % transpTable.length == 0;
        final int blockNum = msg.length() / transpTable.length;
        final char[] decryptedArr = new char[msg.length()];
        for (int i = 0; i < blockNum; i++) {
            for (int j = 0; j < transpTable.length; j++) {
                decryptedArr[i * transpTable.length + transpTable[j]] = msg.charAt(i * transpTable.length + j);
            }
        }
        return new String(decryptedArr);
    }
}
