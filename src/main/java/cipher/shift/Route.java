package cipher.shift;

public enum Route {
    RIGHT_TO_LEFT,
    LEFT_TO_RIGHT,
    UP_TO_DOWN,
    DOWN_TO_UP
}
