package cipher.shift;

import cipher.Cipher;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SimpleShift implements Cipher {

    private int[] transpTable;

    public SimpleShift(int size) {
        transpTable = new int[size];
        final List<Integer> nums = new ArrayList<Integer>();
        for (int i = 0; i < size; i++) {
            nums.add(i);
        }
        final Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < size; i++) {
            transpTable[i] = nums.remove(random.nextInt(nums.size()));
        }
    }

    @Override
    public String encrypt(String msg) {
        assert msg.length() == transpTable.length;
        final StringBuilder encrypted = new StringBuilder();
        for (int i = 0; i < msg.length(); i++) {
            encrypted.append(msg.charAt(transpTable[i]));
        }
        return encrypted.toString();
    }

    @Override
    public String decrypt(String msg) {
        assert msg.length() == transpTable.length;
        final char[] decryptedArr = new char[msg.length()];
        for (int i = 0; i < msg.length(); i++) {
            decryptedArr[transpTable[i]] = msg.charAt(i);
        }
        return new String(decryptedArr);
    }
}
