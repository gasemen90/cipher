package cipher.shift;


import cipher.Cipher;

import java.util.List;

public class VerticalShift implements Cipher {

    private final int[] replacement;
    private final int[] reverse;

    public VerticalShift(String key, List<Character> alphabet) {
        replacement = new int[key.length()];
        int count = 0;
        for (char alpha : alphabet) {
            for (int i = 0; i < key.length(); i++) {
                char ch = key.charAt(i);
                if (ch == alpha) {
                    replacement[i] = count++;
                }
            }
            if (key.length() == count) {
                break;
            }
        }
        assert key.length() == count;
        reverse = new int[replacement.length];
        for (int i = 0; i < replacement.length; i++) {
            reverse[replacement[i]] = i;
        }
    }

    @Override
    public String encrypt(String msg) {
        final int rowNums = (msg.length() - 1) / replacement.length + 1;
        final char[][] table = new char[replacement.length][rowNums];
        for (int i = 0; i < msg.length(); i++) {
            table[replacement[i % replacement.length]][i / replacement.length] = msg.charAt(i);
        }
        final StringBuilder encrypted = new StringBuilder();
        for (char[] arr : table) {
            encrypted.append(new String(arr));
        }
        return encrypted.toString();
    }

    @Override
    public String decrypt(String msg) {
        final char[] decrypted = new char[msg.length()];
        int num = 0;
        for (int colNum : reverse) {
            final int rowNum = (msg.length() - 1) / reverse.length + 1;
            for (int j = 0; j < rowNum; j++) {
                decrypted[j * reverse.length + colNum] = msg.charAt(num++);
            }
        }
        assert num == msg.length();
        return new String(decrypted).trim();
    }
}
