package cipher.shift;

import cipher.Cipher;

import java.util.Arrays;

public class DoubleShift implements Cipher {

    private static final char DUMMY = '_';

    private final Route inscribing;
    private final Route copying;
    private final int[] rowNums;
    private final int[] colNums;
    private final int[] inverseRowNums;
    private final int[] inverseColNums;

    public DoubleShift(Route inscribing, Route copying, int[] rowNums, int[] colNums) {
        this.inscribing = inscribing;
        this.copying = copying;
        this.rowNums = rowNums;
        inverseRowNums = new int[this.rowNums.length];
        for (int i = 0; i < inverseRowNums.length; i++) {
            inverseRowNums[rowNums[i] - 1] = i;
        }
        this.colNums = colNums;
        inverseColNums = new int[this.colNums.length];
        for (int i = 0; i < inverseColNums.length; i++) {
            inverseColNums[colNums[i] - 1] = i;
        }
    }

    @Override
    public String encrypt(String msg) {
        if (msg.length() % colNums.length != 0) {
            final char[] fill = new char[colNums.length - msg.length() % colNums.length];
            Arrays.fill(fill, DUMMY);
            msg += new String(fill);
        }
        assert msg.length() / colNums.length == rowNums.length;
        final char[][] table = inscribe(msg, inscribing);
        final char[][] tableCopy = new char[inverseRowNums.length][inverseColNums.length];
        for (int i = 0; i < inverseRowNums.length; i++) {
            for (int j = 0; j < inverseColNums.length; j++) {
                tableCopy[i][j] = table[inverseRowNums[i]][inverseColNums[j]];
            }
        }
        return copy(tableCopy, copying).trim();
    }

    @Override
    public String decrypt(String msg) {
        assert msg.length() / colNums.length == rowNums.length;
        final char[][] table = inscribe(msg, copying);
        final char[][] copyTable = new char[rowNums.length][colNums.length];
        for (int i = 0; i < rowNums.length; i++) {
            for (int j = 0; j < colNums.length; j++) {
                copyTable[i][j] = table[rowNums[i] - 1][colNums[j] - 1];
            }
        }
        final char[] decrypted = copy(copyTable, inscribing).toCharArray();
        for (int i = decrypted.length - 1; i >= 0; i--) {
            if (decrypted[i] == DUMMY) {
                decrypted[i] = 0;
            }
        }
        return new String(decrypted).trim();
    }

    private char[][] inscribe(String msg, Route route) {
        final char[][] table = new char[rowNums.length][colNums.length];
        for (int i = 0; i < msg.length(); i++) {
            final char ch = msg.charAt(i);
            switch (route) {
                case RIGHT_TO_LEFT:
                    table[i / colNums.length][colNums.length - (i % colNums.length) - 1] = ch;
                    break;
                case LEFT_TO_RIGHT:
                    table[i / colNums.length][i % colNums.length] = ch;
                    break;
                case UP_TO_DOWN:
                    table[i % rowNums.length][i / rowNums.length] = ch;
                    break;
                case DOWN_TO_UP:
                    table[rowNums.length - (i % rowNums.length) - 1][i / rowNums.length] = ch;
                    break;
                default:
                    throw new IllegalStateException();
            }
        }
        return table;
    }

    private String copy(char[][] table, Route route) {
        final StringBuilder msg = new StringBuilder();
        for (int i = 0; i < table.length; i++) {
            assert table[i].length == colNums.length;
            for (int j = 0; j < table[i].length; j++) {
                final int index = i * table[i].length + j;
                switch (route) {
                    case RIGHT_TO_LEFT:
                        msg.append(table[i][table[i].length - j - 1]);
                        break;
                    case LEFT_TO_RIGHT:
                        msg.append(table[i][j]);
                        break;
                    case UP_TO_DOWN:
                        msg.append(table[index % table.length][index / table.length]);
                        break;
                    case DOWN_TO_UP:
                        msg.append(table[table.length - (index % table.length) - 1][index / table.length]);
                        break;
                    default:
                        throw new IllegalStateException();
                }
            }
        }
        return msg.toString().trim();
    }
}
