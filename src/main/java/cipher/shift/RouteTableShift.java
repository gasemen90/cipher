package cipher.shift;

import cipher.Cipher;

public class RouteTableShift implements Cipher {

    private int columns;
    private Route inscribing;
    private Route copying;

    public RouteTableShift(int columns, Route inscribing, Route copying) {
        this.columns = columns;
        this.inscribing = inscribing;
        this.copying = copying;
    }

    @Override
    public String encrypt(String msg) {
        return copy(inscribe(msg, inscribing), copying);
    }

    @Override
    public String decrypt(String msg) {
        return copy(inscribe(msg, copying), inscribing);
    }

    private char[][] inscribe(String msg, Route route) {
        final int rowNums = (msg.length() - 1) / columns + 1;
        final char[][] table = new char[rowNums][columns];
        for (int i = 0; i < msg.length(); i++) {
            final char ch = msg.charAt(i);
            switch (route) {
                case RIGHT_TO_LEFT:
                    table[i / columns][columns - (i % columns) - 1] = ch;
                    break;
                case LEFT_TO_RIGHT:
                    table[i / columns][i % columns] = ch;
                    break;
                case UP_TO_DOWN:
                    table[i % rowNums][i / rowNums] = ch;
                    break;
                case DOWN_TO_UP:
                    table[rowNums - (i % rowNums) - 1][i / rowNums] = ch;
                    break;
                default:
                    throw new IllegalStateException();
            }
        }
        return table;
    }

    private String copy(char[][] table, Route route) {
        final StringBuilder msg = new StringBuilder();
        for (int i = 0; i < table.length; i++) {
            assert table[i].length == columns;
            for (int j = 0; j < table[i].length; j++) {
                final int index = i * table[i].length + j;
                switch (route) {
                    case RIGHT_TO_LEFT:
                        msg.append(table[i][table[i].length - j - 1]);
                        break;
                    case LEFT_TO_RIGHT:
                        msg.append(table[i][j]);
                        break;
                    case UP_TO_DOWN:
                        msg.append(table[index % table.length][index / table.length]);
                        break;
                    case DOWN_TO_UP:
                        msg.append(table[table.length - (index % table.length) - 1][index / table.length]);
                        break;
                    default:
                        throw new IllegalStateException();
                }
            }
        }
        return msg.toString().trim();
    }
}
