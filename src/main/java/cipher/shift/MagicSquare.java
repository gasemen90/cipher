package cipher.shift;

import cipher.Cipher;

import java.util.Arrays;

public class MagicSquare implements Cipher {

    private static final char DOT = '.';

    private int[][] square;

    public MagicSquare(int[][] square) {
        this.square = square;
        for (int[] line : square) {
            assert line.length == square.length;
        }
    }

    @Override
    public String encrypt(String msg) {
        if (square.length * square.length > msg.length()) {
            final char[] fill = new char[square.length * square.length - msg.length()];
            Arrays.fill(fill, DOT);
            msg += new String(fill);
        }
        final char[] encrypted = new char[msg.length()];
        for (int i = 0; i < msg.length(); i++) {
            encrypted[i] = msg.charAt(square[i / square.length][i % square.length] - 1);
        }
        return new String(encrypted);
    }

    @Override
    public String decrypt(String msg) {
        final char[] decrypted = new char[msg.length()];
        for (int i = 0; i < decrypted.length; i++) {
            decrypted[square[i / square.length][i % square.length] - 1] = msg.charAt(i);
        }
        for (int i = decrypted.length - 1; i >= 0; i--) {
            if (decrypted[i] == DOT) {
                decrypted[i] = 0;
            } else {
                break;
            }
        }
        return new String(decrypted).trim();
    }
}
