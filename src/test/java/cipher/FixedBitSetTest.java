package cipher;


import cipher.symmetric.FixedBitSet;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FixedBitSetTest {

    private static final int SIZE = 8;

    @Test
    public void flipTest() {
        FixedBitSet bs1 = new FixedBitSet(SIZE);
        bs1.set(0);
        bs1.set(2);
        bs1.set(3);
        bs1.set(4);
        bs1.flip(0);
        bs1.flip(1);

        FixedBitSet bs2 = new FixedBitSet(SIZE);
        bs2.set(1);
        bs2.set(2);
        bs2.set(3);
        bs2.set(4);

        Assert.assertEquals(bs1, bs2);
    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    public void flipIndexOutOfBoundsTest() {
        new FixedBitSet(SIZE).flip(SIZE);
    }

    @Test
    public void flipRangeTest() {
        FixedBitSet bs1 = new FixedBitSet(SIZE);
        bs1.set(0);
        bs1.set(2);
        bs1.set(3);
        bs1.flip(0, 3);

        FixedBitSet bs2 = new FixedBitSet(SIZE);
        bs2.set(1);
        bs2.set(3);

        Assert.assertEquals(bs1, bs2);
    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    public void flipRangeFromIndexOutOfBoundsTest() {
        new FixedBitSet(SIZE).flip(SIZE, SIZE);
    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    public void flipRangeToIndexOutOfBoundsTest() {
        new FixedBitSet(SIZE).flip(0, SIZE + 1);
    }

    @Test
    public void setTest() {
        FixedBitSet bs1 = new FixedBitSet(SIZE);
        bs1.set(SIZE - 1);
        Assert.assertEquals(bs1.get(SIZE - 1), true);
    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    public void setIndexOutOfBoundsTest() {
        new FixedBitSet(SIZE).set(SIZE);
    }

    @Test
    public void setValueTest() {
        FixedBitSet bs1 = new FixedBitSet(SIZE);
        bs1.set(SIZE - 1, true);
        Assert.assertEquals(bs1.get(SIZE - 1), true);

        FixedBitSet bs2 = new FixedBitSet(SIZE);
        bs2.set(SIZE - 1, false);
        Assert.assertEquals(bs2.get(SIZE - 1), false);

        FixedBitSet bs3 = new FixedBitSet(SIZE);
        bs3.set(SIZE - 1, true);
        bs3.set(SIZE - 1, false);
        Assert.assertEquals(bs3.get(SIZE - 1), false);

        FixedBitSet bs4 = new FixedBitSet(SIZE);
        bs4.set(SIZE - 1, false);
        bs4.set(SIZE - 1, true);
        Assert.assertEquals(bs4.get(SIZE - 1), true);
    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    public void setValueIndexOutOfBoundsTest() {
        new FixedBitSet(SIZE).set(SIZE, true);
    }

    @Test
    public void setRangeTest() {
        FixedBitSet bs1 = new FixedBitSet(SIZE);
        bs1.set(1, 4);

        FixedBitSet bs2 = new FixedBitSet(SIZE);
        bs2.set(1);
        bs2.set(2);
        bs2.set(3);

        Assert.assertEquals(bs1, bs2);
    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    public void setRangeIndexOutOfBoundsTest() {
        new FixedBitSet(SIZE).set(0, SIZE + 1);
    }

    @Test
    public void setValueRangeTest() {
        FixedBitSet bs11 = new FixedBitSet(SIZE);
        bs11.set(1, 3, true);
        FixedBitSet bs12 = new FixedBitSet(SIZE);
        bs12.set(1);
        bs12.set(2);
        Assert.assertEquals(bs11, bs12);

        FixedBitSet bs2 = new FixedBitSet(SIZE);
        bs2.set(1, 3, false);
        Assert.assertEquals(bs2, new FixedBitSet(SIZE));

        FixedBitSet bs3 = new FixedBitSet(SIZE);
        bs3.set(1, 3, true);
        bs3.set(1, 3, false);
        Assert.assertEquals(bs3, new FixedBitSet(SIZE));

        FixedBitSet bs41 = new FixedBitSet(SIZE);
        bs41.set(1, 3, false);
        bs41.set(1, 3, true);
        FixedBitSet bs42 = new FixedBitSet(SIZE);
        bs42.set(1);
        bs42.set(2);
        Assert.assertEquals(bs41, bs42);
    }

    @Test
    public void clearTest() {
        FixedBitSet bitSet = new FixedBitSet(SIZE);
        bitSet.set(0, SIZE);
        bitSet.clear();
        Assert.assertEquals(bitSet, new FixedBitSet(SIZE));
    }

    @Test
    public void clearIndexTest() {
        FixedBitSet bs1 = new FixedBitSet(SIZE);
        bs1.set(0, SIZE);
        bs1.clear(0);
        FixedBitSet bs2 = new FixedBitSet(SIZE);
        bs2.set(1, SIZE);
        Assert.assertEquals(bs1, bs2);

        FixedBitSet bs3 = new FixedBitSet(SIZE);
        bs3.set(0, SIZE);
        bs3.clear(SIZE - 1);
        FixedBitSet bs4 = new FixedBitSet(SIZE);
        bs4.set(0, SIZE - 1);
        Assert.assertEquals(bs3, bs4);

        FixedBitSet bs5 = new FixedBitSet(3);
        bs5.set(0, 3);
        bs5.clear(0);
        bs5.clear(2);

        FixedBitSet bs6 = new FixedBitSet(3);
        bs6.set(1);
        Assert.assertEquals(bs5, bs6);
    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    public void clearIndexOutOfBoundsTest() {
        new FixedBitSet(SIZE).clear(SIZE + 1);
    }

    @Test
    public void clearRangeTest() {
        FixedBitSet bs1 = new FixedBitSet(SIZE);
        bs1.set(0, SIZE);
        bs1.clear(0, 2);
        FixedBitSet bs2 = new FixedBitSet(SIZE);
        bs2.set(2, SIZE);
        Assert.assertEquals(bs1, bs2);

        FixedBitSet bs3 = new FixedBitSet(SIZE);
        bs3.set(0, SIZE);
        bs3.clear(SIZE - 2, SIZE);
        FixedBitSet bs4 = new FixedBitSet(SIZE);
        bs4.set(0, SIZE - 2);
        Assert.assertEquals(bs3, bs4);

        FixedBitSet bs5 = new FixedBitSet(4);
        bs5.set(0, 4);
        bs5.clear(1, 3);
        FixedBitSet bs6 = new FixedBitSet(4);
        bs6.set(0);
        bs6.set(3);
        Assert.assertEquals(bs5, bs6);
    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    public void clearRangeIndexOutOfBoundsTest() {
        new FixedBitSet(SIZE).clear(0, SIZE + 1);
    }

    @Test
    public void getTest() {
        FixedBitSet bs1 = new FixedBitSet(SIZE);
        bs1.set(3);
        Assert.assertNotEquals(bs1.get(4), true);

        FixedBitSet bs2 = new FixedBitSet(SIZE);
        bs2.set(4);
        Assert.assertEquals(bs2.get(4), true);
    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    public void getIndexOutOfBounds() {
        new FixedBitSet(SIZE).get(SIZE);
    }

    @Test
    public void getRangeTest() {
        FixedBitSet bs1 = new FixedBitSet(SIZE);
        bs1.set(0);
        bs1.set(2);
        bs1.set(4);
        FixedBitSet bs2 = new FixedBitSet(3);
        bs2.set(1);
        Assert.assertEquals(bs1.get(1, 4), bs2);
    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    public void getRangeIndexOutOfBounds() {
        new FixedBitSet(SIZE).get(0, SIZE + 1);
    }

    @Test
    public void lengthTest() {
        Assert.assertEquals(new FixedBitSet(0).length(), 0);
        Assert.assertEquals(new FixedBitSet(3).length(), 3);
    }

    @Test
    public void appendRightTest() {
        FixedBitSet bs1 = new FixedBitSet(3);
        bs1.set(2);
        FixedBitSet bs2 = new FixedBitSet(5);
        bs2.set(1);
        bs2.set(4);
        bs1.appendRight(bs2);

        FixedBitSet bs3 = new FixedBitSet(8);
        bs3.set(1);
        bs3.set(4);
        bs3.set(7);
        Assert.assertEquals(bs1, bs3);
    }

    @Test
    public void appendValueRightTest() {
        FixedBitSet bs1 = new FixedBitSet(3);
        bs1.set(2);
        bs1.appendRight(true);

        FixedBitSet bs2 = new FixedBitSet(4);
        bs2.set(0);
        bs2.set(3);

        Assert.assertEquals(bs1, bs2);
    }

    @Test
    public void appendLeftTest() {
        FixedBitSet bs1 = new FixedBitSet(3);
        bs1.set(2);

        FixedBitSet bs2 = new FixedBitSet(2);
        bs2.set(1);
        bs1.appendLeft(bs2);

        FixedBitSet bs3 = new FixedBitSet(5);
        bs3.set(2);
        bs3.set(4);

        Assert.assertEquals(bs1, bs3);
    }

    @Test
    public void appendValueLeft() {
        FixedBitSet bs1 = new FixedBitSet(3);
        bs1.set(2);
        bs1.appendLeft(false);

        FixedBitSet bs2 = new FixedBitSet(4);
        bs2.set(2);
        Assert.assertEquals(bs1, bs2);
    }

    @Test
    public void shiftRightTest() {
        FixedBitSet bs1 = new FixedBitSet(SIZE);
        bs1.set(2, 4);
        bs1.shiftRight(1);
        Assert.assertEquals(bs1.toString(), getBinaryStr(0b1100 >> 1, SIZE));

        FixedBitSet bs2 = new FixedBitSet(SIZE);
        bs2.set(2, 4);
        bs2.shiftRight(3);
        Assert.assertEquals(bs2.toString(), getBinaryStr(0b1100 >> 3, SIZE));
    }

    @Test
    public void shiftLeftTest() {
        FixedBitSet bs1 = new FixedBitSet(SIZE);
        bs1.set(2, 4);
        bs1.shiftLeft(1);
        Assert.assertEquals(bs1.toString(), getBinaryStr(0b1100 << 1, SIZE));

        FixedBitSet bs2 = new FixedBitSet(SIZE);
        bs2.set(2, 4);
        bs2.shiftLeft(3);
        Assert.assertEquals(bs2.toString(), getBinaryStr(0b1100 << 3, SIZE));
    }

    @Test
    public void rotateRightTest() {
        FixedBitSet bs = new FixedBitSet(32);
        bs.set(0);
        bs.set(30);
        bs.rotateRight(2);
        Assert.assertEquals(bs.toString(), getBinaryStr(Integer.rotateRight(0x40000001, 2), 32));
    }

    @Test
    public void rotateLeftTest() {
        FixedBitSet bs = new FixedBitSet(32);
        bs.set(0);
        bs.set(30);
        bs.rotateLeft(2);
        Assert.assertEquals(bs.toString(), getBinaryStr(Integer.rotateLeft(0x40000001, 2), 32));
    }

    @Test
    public void toStringTest() {
        FixedBitSet f1 = new FixedBitSet(4);
        f1.set(1);
        f1.set(3);
        Assert.assertEquals(f1.toString(), "1010");

        Assert.assertEquals(new FixedBitSet(SIZE).toString(), new String(new char[SIZE]).replace('\0', '0'));

        FixedBitSet f2 = new FixedBitSet(SIZE);
        f2.set(0, SIZE);
        Assert.assertEquals(f2.toString(), new String(new char[SIZE]).replace('\0', '1'));
    }

    @Test
    public void cloneTest() {
        FixedBitSet bs = new FixedBitSet(SIZE);
        bs.set(0);
        bs.set(SIZE / 2);
        bs.set(SIZE - 1);
        Assert.assertEquals(bs, bs.clone());
    }

    private static String getBinaryStr(long num, int n) {
        return String.format("%" + n + "." + n + "s", Long.toBinaryString(num)).replace(' ', '0');
    }
}
