package cipher;

import cipher.symmetric.BitArray;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;

public class BitArrayTest {

    private static final int SIZE = 8;

    @Test
    public void sizeConstructorTest() {
        final BitArray bitArray = new BitArray(SIZE);
        Assert.assertEquals(bitArray.length(), SIZE);
        Assert.assertEquals(bitArray.toString(), new String(new char[SIZE]).replace("\0", "0"));
    }

    @Test
    public void arrConstructorTest() {
        final BitArray bitArray1 = new BitArray(new boolean[SIZE]);
        Assert.assertEquals(bitArray1.length(), SIZE);
        Assert.assertEquals(bitArray1.toString(), new String(new char[SIZE]).replace("\0", "0"));

        final boolean[] arr = new boolean[] {true, true, false, true};
        final BitArray bitArray2 = new BitArray(arr);
        Assert.assertEquals(bitArray2.length(), arr.length);
        Assert.assertEquals(bitArray2.toString(), "1011");
    }

    @Test
    public void byteArrayConstructorTest() {
        Assert.assertEquals(new BitArray(new byte[] { 0b110011, 0b1111 }).toString(), "0000111100110011");
    }

    @Test
    public void bitArrayConstructorTest() {
        final BitArray bitArray1 = new BitArray(new BitArray(SIZE));
        Assert.assertEquals(bitArray1.length(), SIZE);
        Assert.assertEquals(bitArray1.toString(), new String(new char[SIZE]).replace("\0", "0"));

        final BitArray expect = new BitArray(SIZE);
        expect.set(0);
        expect.set(7);
        final BitArray bitArray2 = new BitArray(expect);
        Assert.assertEquals(bitArray2.length(), expect.length());
        Assert.assertEquals(bitArray2.toString(), "10000001");
    }

    @Test
    public void appendRightTest() {
        Assert.assertEquals(new BitArray(3).set(2).appendRight(new BitArray(5).set(1).set(4)), new BitArray(8).set(1).set(4).set(7));
    }

    @Test
    public void appendRightValue() {
        Assert.assertEquals(new BitArray(3).set(2).appendRight(true), new BitArray(4).set(0).set(3));
    }

    @Test
    public void appendLeftTest() {
        Assert.assertEquals(new BitArray(3).set(2).appendLeft(new BitArray(2).set(1)), new BitArray(5).set(2).set(4));
    }

    @Test
    public void appendLeftValue() {
        Assert.assertEquals(new BitArray(3).set(2).appendLeft(false), new BitArray(4).set(2));
    }

    @Test
    public void andTest() {
        final BitArray bitArray = new BitArray(SIZE);
        bitArray.set(0).set(2);
        final BitArray bitArray2 = new BitArray(SIZE + SIZE);
        bitArray2.set(0).set(1).set(SIZE);
        final BitArray result = bitArray.and(bitArray2);
        Assert.assertEquals(result.length(), SIZE);
        Assert.assertEquals(result, new BitArray(SIZE).set(0));
        Assert.assertEquals(result.toString(), getBinaryStr(0b0101 & 0b0011, 8));
        final BitArray result2 = bitArray2.and(bitArray);
        Assert.assertEquals(result2.length(), SIZE);
        Assert.assertEquals(result2, new BitArray(SIZE).set(0));
    }

    @Test
    public void andNotTest() {
        final BitArray bitArray = new BitArray(SIZE);
        bitArray.set(0).set(2);
        final BitArray bitArray2 = new BitArray(SIZE + SIZE);
        bitArray2.set(0).set(1).set(SIZE);
        final BitArray result = bitArray.andNot(bitArray2);
        Assert.assertEquals(result, new BitArray(SIZE).set(2));
        final BitArray result2 = bitArray2.andNot(bitArray);
        Assert.assertEquals(result2, new BitArray(SIZE + SIZE).set(SIZE).set(1));
    }

    @Test
    public void cardinalityTest() {
        Assert.assertEquals(new BitArray(SIZE).cardinality(), 0);
        Assert.assertEquals(new BitArray(SIZE).set(0, SIZE).cardinality(), SIZE);
        Assert.assertEquals(new BitArray(SIZE).set(0).set(3).set(5).cardinality(), 3);
    }

    @Test
    public void cardinalityRangeTest() {
        Assert.assertEquals(new BitArray(SIZE).cardinality(0, SIZE), 0);
        Assert.assertEquals(new BitArray(SIZE).set(1, SIZE).cardinality(0, 1), 0);
        Assert.assertEquals(new BitArray(SIZE).set(0, SIZE).cardinality(0, SIZE), 8);
        Assert.assertEquals(new BitArray(SIZE).set(0).set(2).set(4).cardinality(0, 3), 2);
    }

    @Test
    public void clearTest() {
        Assert.assertEquals(new BitArray(SIZE).set(0, SIZE).clear(), new BitArray(SIZE));
    }

    @Test
    public void clearIndexTest() {
        Assert.assertEquals(new BitArray(SIZE).set(0, SIZE).clear(0), new BitArray(SIZE).set(1, SIZE));
        Assert.assertEquals(new BitArray(SIZE).set(0, SIZE).clear(SIZE - 1), new BitArray(SIZE).set(0, SIZE - 1));
        Assert.assertEquals(new BitArray(3).set(0, 3).clear(0).clear(2), new BitArray(3).set(1));
    }

    @Test
    public void clearRangeTest() {
        Assert.assertEquals(new BitArray(SIZE).set(0, SIZE).clear(0, 2), new BitArray(SIZE).set(2, SIZE));
        Assert.assertEquals(new BitArray(SIZE).set(0, SIZE).clear(SIZE - 2, SIZE), new BitArray(SIZE).set(0, SIZE - 2));
        Assert.assertEquals(new BitArray(4).set(0, 4).clear(1, 3), new BitArray(4).set(0).set(3));
    }

    @Test
    public void cloneTest() {
        final BitArray bitArray = new BitArray(SIZE).set(0).set(2).set(3).set(4);
        Assert.assertEquals(bitArray, bitArray.clone());
    }

    @Test
    public void flipTest() {
        Assert.assertEquals(new BitArray(SIZE).set(0).set(2).set(3).set(4).flip(0).flip(1), new BitArray(SIZE).set(1).set(2).set(3).set(4));
    }

    @Test
    public void flipRangeTest() {
        Assert.assertEquals(new BitArray(SIZE).set(0).set(2).set(3).flip(0, 3), new BitArray(SIZE).set(1).set(3));
    }

    @Test
    public void getTest() {
        Assert.assertNotEquals(new BitArray(SIZE).set(3).get(4), true);
        Assert.assertEquals(new BitArray(SIZE).set(4).get(4), true);
    }

    @Test(expectedExceptions = ArrayIndexOutOfBoundsException.class)
    public void getTestIndexOutOfBounds() {
        new BitArray(SIZE).get(SIZE);
    }

    @Test
    public void getRangeTest() {
        Assert.assertEquals(new BitArray(SIZE).set(0).set(2).set(4).get(1, 4), new BitArray(3).set(1));
    }

    @Test
    public void hashCodeTest() {
        final boolean[] arr = new boolean[] {true, false, false, true};
        Assert.assertEquals(new BitArray(arr).hashCode(), Arrays.hashCode(arr));
    }

    @Test
    public void isEmptyTest() {
        Assert.assertTrue(new BitArray(SIZE).isEmpty());
        Assert.assertFalse(new BitArray(SIZE).set(SIZE / 2).isEmpty());
    }

    @Test
    public void lengthTest() {
        Assert.assertEquals(new BitArray(0).length(), 0);
        Assert.assertEquals(new BitArray(new boolean[0]).length(), 0);
        Assert.assertEquals(new BitArray(new BitArray(0)).length(), 0);

        Assert.assertEquals(new BitArray(3).length(), 3);
        Assert.assertEquals(new BitArray(new boolean[3]).length(), 3);
        Assert.assertEquals(new BitArray(new BitArray(3)).length(), 3);
    }

    @Test
    public void orTest() {
        final BitArray bitArray = new BitArray(3).set(0).set(2);
        final BitArray bitArray2 = new BitArray(SIZE).set(1);
        Assert.assertEquals(bitArray.or(bitArray2), new BitArray(SIZE).set(0).set(1).set(2));
        Assert.assertEquals(bitArray2.or(bitArray), new BitArray(SIZE).set(0).set(1).set(2));
    }

    @Test
    public void setTest() {
        Assert.assertEquals(new BitArray(SIZE).set(SIZE - 1).get(SIZE - 1), true);
    }

    @Test
    public void setValueTest() {
        Assert.assertEquals(new BitArray(SIZE).set(SIZE - 1, true).get(SIZE - 1), true);
        Assert.assertEquals(new BitArray(SIZE).set(SIZE - 1, false).get(SIZE - 1), false);
        Assert.assertEquals(new BitArray(SIZE).set(SIZE - 1, true).set(SIZE - 1, false).get(SIZE - 1), false);
        Assert.assertEquals(new BitArray(SIZE).set(SIZE - 1, false).set(SIZE - 1, true).get(SIZE - 1), true);
    }

    @Test
    public void setRangeTest() {
        Assert.assertEquals(new BitArray(SIZE).set(1, 4), new BitArray(SIZE).set(1).set(2).set(3));
    }

    @Test
    public void setRangeValue() {
        Assert.assertEquals(new BitArray(SIZE).set(1, 3, true), new BitArray(SIZE).set(1).set(2));
        Assert.assertEquals(new BitArray(SIZE).set(1, 3, false), new BitArray(SIZE));
        Assert.assertEquals(new BitArray(SIZE).set(1, 3, true).set(1, 3, false), new BitArray(SIZE));
        Assert.assertEquals(new BitArray(SIZE).set(1, 3, false).set(1, 3, true), new BitArray(SIZE).set(1).set(2));
    }

    @Test
    public void toStringTest() {
        Assert.assertEquals(new BitArray(4).set(1).set(3).toString(), "1010");
        Assert.assertEquals(new BitArray(SIZE).toString(), new String(new char[SIZE]).replace('\0', '0'));
        Assert.assertEquals(new BitArray(SIZE).set(0, SIZE).toString(), new String(new char[SIZE]).replace('\0', '1'));
    }

    @Test
    public void xorTest() {
        final BitArray bitArray = new BitArray(8).set(0, 2);
        final BitArray bitArray2 = new BitArray(4).set(1).set(3);
        Assert.assertEquals(bitArray.xor(bitArray2).toString(), getBinaryStr(0b11 ^ 0b1010, 8));
        Assert.assertEquals(bitArray2.xor(bitArray).toString(), getBinaryStr(0b11 ^ 0b1010, 8));
        Assert.assertEquals(new BitArray(SIZE).xor(new BitArray(SIZE)).toString(), getBinaryStr(0, 8));
        Assert.assertEquals(new BitArray(SIZE).set(0, SIZE).xor(new BitArray(SIZE)).toString(), getBinaryStr(0b11111111, 8));
    }

    @Test
    public void shiftRightTest() {
        Assert.assertEquals(new BitArray(SIZE).set(2, 4).shiftRight(1).toString(), getBinaryStr(0b1100 >> 1, SIZE));
        Assert.assertEquals(new BitArray(SIZE).set(2, 4).shiftRight(3).toString(), getBinaryStr(0b1100 >> 3, SIZE));
    }

    @Test
    public void shiftLeftTest() {
        Assert.assertEquals(new BitArray(SIZE).set(2, 4).shiftLeft(1).toString(), getBinaryStr(0b1100 << 1, SIZE));
        Assert.assertEquals(new BitArray(SIZE).set(2, 4).shiftLeft(3).toString(), getBinaryStr(0b1100 << 3, SIZE));
    }

    @Test
    public void rotateRightTest() {
        final BitArray bitArray = new BitArray(32).set(0).set(30);
        final int val = 0x40000001;
        Assert.assertEquals(bitArray.rotateRight(2).toString(), getBinaryStr(Integer.rotateRight(val, 2), 32));
    }

    @Test
    public void rotateLeftTest() {
        final BitArray bitArray = new BitArray(32).set(0).set(30);
        final int val = 0x40000001;
        Assert.assertEquals(bitArray.rotateLeft(2).toString(), getBinaryStr(Integer.rotateLeft(val, 2), 32));
    }

    private static String getBinaryStr(long num, int n) {
        return String.format("%" + n + "." + n + "s", Long.toBinaryString(num)).replace(' ', '0');
    }

}
