package cipher;

import cipher.additive.SumByMod2;
import cipher.additive.SumByModN;
import cipher.publickey.ElGamal;
import cipher.publickey.Knapsack;
import cipher.publickey.rsa.RSA;
import cipher.shift.DoubleShift;
import cipher.shift.Grid;
import cipher.shift.MagicSquare;
import cipher.shift.Route;
import cipher.shift.RouteTableShift;
import cipher.shift.SimpleBlockShift;
import cipher.shift.SimpleShift;
import cipher.shift.VerticalShift;
import cipher.substitution.Caesar;
import cipher.substitution.Homophone;
import cipher.substitution.Playfair;
import cipher.substitution.PolybiusSquare;
import cipher.substitution.Slogan;
import cipher.substitution.Trismus;
import cipher.substitution.Vigenere;
import cipher.symmetric.DesEcb;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static java.util.Arrays.asList;

public class CipherTest {
    protected static final Logger log = LoggerFactory.getLogger(CipherTest.class);

    static final String SECRET_KEY = "Секретный ключ";
    static final String MESSAGE = "Секретное сообщение";
    static final List<Character> ALPHABET = Collections.unmodifiableList(asList(
            'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О',
            'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', ' '));

    @DataProvider(name = "test")
    public static Object[][] data() {
        return new Object[][]{
                // Substitution
                { new Caesar(3, ALPHABET),                             MESSAGE.toUpperCase() },
                { new Slogan(SECRET_KEY.toUpperCase(), ALPHABET) ,     MESSAGE.toUpperCase() },
                { new PolybiusSquare(6, ALPHABET),                     MESSAGE.toUpperCase() },
                { new Trismus(SECRET_KEY.toUpperCase(), 8, ALPHABET),  MESSAGE.toUpperCase() },
                { new Playfair("СЕКРЕТ", 6, ALPHABET.subList(0, 30)),  getRandom(ALPHABET.subList(0, 30)) },
                { new Homophone(ALPHABET),                             MESSAGE.toUpperCase()},
                { new Vigenere(ALPHABET) ,                             MESSAGE.toUpperCase()},
                // Shift
                { new SimpleShift(MESSAGE.length()), MESSAGE },
                { new SimpleBlockShift(3), "СЕКРЕТ" },
                { new RouteTableShift(6, Route.RIGHT_TO_LEFT, Route.UP_TO_DOWN), MESSAGE },
                { new VerticalShift("ДЯДИНА", ALPHABET), MESSAGE},
                { new Grid(new boolean[][] {
                            { true, false, true, false },
                            { false, false, false, false },
                            { false, true, false, true },
                            { false, false, false, false }
                        }, new Grid.Rotate[] { Grid.Rotate.H, Grid.Rotate.V, Grid.Rotate.H }, Route.RIGHT_TO_LEFT), "АБРАМОВ+ДЯДИНА" },
                { new Grid(new boolean[][] {
                            { true, false, true, false },
                            { false, false, false, false },
                            { false, true, false, true },
                            { false, false, false, false }
                        }, new Grid.Rotate[] { Grid.Rotate.H, Grid.Rotate.V, Grid.Rotate.H }, Route.LEFT_TO_RIGHT), "АБРАМОВ+ДЯДИНА" },
                { new Grid(new boolean[][] {
                            { true, false, true, false },
                            { false, false, false, false },
                            { false, true, false, true },
                            { false, false, false, false }
                        }, new Grid.Rotate[] { Grid.Rotate.H, Grid.Rotate.V, Grid.Rotate.H }, Route.UP_TO_DOWN), "АБРАМОВ+ДЯДИНА" },
                { new Grid(new boolean[][] {
                            { true, false, true, false },
                            { false, false, false, false },
                            { false, true, false, true },
                            { false, false, false, false }
                        }, new Grid.Rotate[] { Grid.Rotate.H, Grid.Rotate.V, Grid.Rotate.H }, Route.DOWN_TO_UP), "АБРАМОВ+ДЯДИНА" },
                { new MagicSquare(new int[][] {
                            { 16, 3, 2, 13 },
                            { 5, 10, 11, 8 },
                            { 9, 6, 7, 12 },
                            { 4, 15, 14, 1 }
                        }) , "АБРАМОВДЯДИНА" },
                { new DoubleShift(Route.LEFT_TO_RIGHT, Route.UP_TO_DOWN, new int[] { 3, 1, 4, 2 }, new int[] { 4, 1, 3, 2 }), "АБРАМОВ+ДЯДИНА" },
                // Additive
                { new SumByModN(SECRET_KEY.toUpperCase(), ALPHABET), MESSAGE.toUpperCase() },
                { new SumByMod2(SECRET_KEY), MESSAGE },
                // Symmetric
                { new DesEcb("OLGA....".getBytes(StandardCharsets.US_ASCII)), MESSAGE },
                // Public key
                { new RSA(),      MESSAGE },
                { new Knapsack(), MESSAGE },
                { new ElGamal(), MESSAGE },
        };
    }

    @Test(dataProvider = "test")
    public void test(Cipher cipher, String msg) throws ReflectiveOperationException {
        log.debug(cipher.getClass().getName());
        final String encryptedMsg = cipher.encrypt(msg);
        Assert.assertNotEquals(msg, encryptedMsg);
        Assert.assertEquals(cipher.decrypt(encryptedMsg), msg);
    }

    private static String getRandom(List<Character> alphabet) {
        final List<Character> shuffled = new ArrayList<Character>(alphabet);
        Collections.shuffle(shuffled, new Random(System.currentTimeMillis()));
        final StringBuilder result = new StringBuilder();
        for (Character ch : shuffled) {
            result.append(ch);
        }
        return result.toString();
    }
}
